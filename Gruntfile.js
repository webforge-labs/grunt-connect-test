/*global module:false*/
module.exports = function(grunt) {
  
  grunt.initConfig({
  });

  grunt.registerTask('default', ['publish']);

  grunt.task.registerTask('publish', "prepares the relase and publishes with npm", function () {
    var npm = require('npm');
    var process = require('process');

    if (!process.env.TRAVIS_SECURE_ENV_VARS || process.env.TRAVIS_SECURE_ENV_VARS === 'false') {
      grunt.log.ok('Will not do something when secure vars are not set. (Travis Pull Request)');
      return 0;
    }

    var npmconfig = {
      username: process.env.NPM_USERNAME,
      password: process.env.NPM_PASSWORD,
      email: process.env.NPM_EMAIL
    };

    var done = this.async();

      npm.load({}, function(err) {
        npm.registry.adduser(npmconfig.username, npmconfig.password, npmconfig.email, function(err) {

          if (err) {
            grunt.log.error(err);
            done(false);
          } else {
            npm.config.set("email", npmconfig.email, "user");
            npm.config.set("tag", "dev", "user");

            grunt.log.ok('success');

            done(true);
          }
        });
      });
  });
};